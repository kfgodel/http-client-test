package info.kfgodel.javaclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Date: 31/5/20 - 16:18
 */
public class Requester implements Runnable {
  private URL url;
  private RequestCounter counter;

  public Requester(URL url, RequestCounter counter) {
    this.url = url;
    this.counter = counter;
  }

  @Override
  public void run() {
    while (true){
      makeRequest();
      counter.increment();
    }
  }

  private void makeRequest() {
    try {
      final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod("GET");
      BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
      while (in.readLine() != null) {
        // Simulate consuming response
      }
      in.close();
      connection.disconnect();
    } catch (IOException e) {
      throw new RuntimeException("Boom MF", e);
    }
  }
}
