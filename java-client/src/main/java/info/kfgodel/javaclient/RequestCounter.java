package info.kfgodel.javaclient;

/**
 * Date: 31/5/20 - 16:19
 */
public class RequestCounter {

  private volatile long requestAccumulator = 0L;

  public long count(){
    long current = requestAccumulator;
    requestAccumulator -= current;
    return current;
  }

  public void increment() {
    requestAccumulator++;
  }
}
