package info.kfgodel.javaclient;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Date: 31/5/20 - 16:38
 */
public class Spawner extends Thread {

  public volatile int amountOfRequesters = 0;

  private final RequestCounter counter;
  private final URL url;
  private List<Thread> spawned = new ArrayList<>();

  public Spawner(URL url, RequestCounter counter) {
    this.url = url;
    this.counter = counter;
  }


  @Override
  public void run() {
    for (int i = 0; i < 10_000; i++) {
      spawnRequester();
      amountOfRequesters++;
      try {
        Thread.sleep(10_000);
      } catch (InterruptedException e) {
        break;
      }
    }
    spawned.forEach(Thread::interrupt);
  }

  private void spawnRequester() {
    final Thread requesterThread = new Thread(new Requester(url, counter));
    spawned.add(requesterThread);
    requesterThread.start();
  }
}
