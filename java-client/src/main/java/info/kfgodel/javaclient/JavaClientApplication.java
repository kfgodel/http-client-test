package info.kfgodel.javaclient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;

public class JavaClientApplication {
	public static final Logger LOG = LoggerFactory.getLogger(JavaClientApplication.class);

	public static void main(String[] args) throws InterruptedException, MalformedURLException {
		final URL url = new URL("http://localhost:8082/");
		final RequestCounter counter = new RequestCounter();
		final Spawner spawner = new Spawner(url, counter);

		spawner.start();

		for (int i = 0; i < 10_000_000; i++) {
			LOG.info("Request made: {}, requesters: {}", counter.count(), spawner.amountOfRequesters);
			Thread.sleep(1000);
		}
	}
}
