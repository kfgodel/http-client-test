import { serve } from 'https://deno.land/std@0.53.0/http/server.ts';

const port = 8082
const s = serve({ port });

console.log(`http://localhost:${port}/`);

for await (const req of s) {
  req.respond({ body: "ok" });
}