package info.kgodel.kotlinclient

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import java.net.URL

/**
 * Date: 31/5/20 - 14:14
 */
private val logger = KotlinLogging.logger {}
fun main() = runBlocking {
  val url = URL("http://localhost:8082/")
  val counter = RequestCounter()
  var amountOfRequesters = 0

  launch {
    repeat(10_000) {
      runWithIoCoroutine(Requester(url, counter), this)
      amountOfRequesters++
      delay(10_000)
    }
  }

  repeat(10_000_000) {
    logger.info("Request made: ${counter.count()}, requesters: $amountOfRequesters")
    delay(1000)
  }
}

fun runWithIoCoroutine(requester: Requester, scope: CoroutineScope) {
  scope.launch(Dispatchers.IO) {
    requester.requestLoop()
  }
}
