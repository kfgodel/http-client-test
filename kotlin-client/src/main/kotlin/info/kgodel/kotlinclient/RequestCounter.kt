package info.kgodel.kotlinclient

/**
 * Date: 31/5/20 - 14:34
 */
class RequestCounter {

    @Volatile
    var requestAccumulator = 0L

    fun count(): Long {
        val current = requestAccumulator
        requestAccumulator -= current
        return current
    }

  fun increment() {
    requestAccumulator++
  }
}