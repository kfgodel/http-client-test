package info.kgodel.kotlinclient

import java.net.URL

/**
 * Date: 31/5/20 - 14:42
 */
class Requester(private val url: URL, private val counter: RequestCounter) {
  fun requestLoop() {
    while (true){
      url.readBytes()
      counter.increment()
    }
  }
}